//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using UnityEngine;

namespace Game
{
	[System.Serializable]
	public class UltColliderFloatEvent : UltEvents.UltEvent<Collider, float> { }
    [System.Serializable]
    public class UltCollider2DFloatEvent : UltEvents.UltEvent<Collider2D, float> { }
}