using UnityEngine;

namespace Game
{
	public class Pushable : MonoBehaviour
	{
		public float forceMultiplier = 1f;
		public UltVector3Event onPush = default;

		[SerializeField] bool freezeX = false, freezeY = false, freezeZ = false;

		bool canPush = true;

		public void Push(Vector3 pusher, float force)
		{
			if (!canPush) return;
			Vector3 dir = Vector3.Normalize(transform.position - pusher);
			dir = FreezeVector(transform.position + dir * force * forceMultiplier, freezeX, freezeY, freezeZ);
			onPush?.Invoke(dir);
		}

		public Vector3 FreezeVector(Vector3 vector3, bool freezeX, bool freezeY, bool freezeZ)
		{
			vector3.x = freezeX ? transform.position.x : vector3.x;
			vector3.y = freezeY ? transform.position.y : vector3.y;
			vector3.z = freezeZ ? transform.position.z : vector3.z;
			return vector3;
		}

		public void SetPushable(bool value) => canPush = value;
	}
}