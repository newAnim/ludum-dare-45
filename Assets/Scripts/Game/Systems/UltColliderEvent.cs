//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using UnityEngine;

namespace Game
{
	[System.Serializable]
	public class UltColliderEvent : UltEvents.UltEvent<Collider> { }
}