﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using UnityAtoms;
using UnityEngine;
using UnityEngine.Events;

namespace Game
{
	[RequireComponent(typeof(Health))]
	public class HealthRegenerate : AtomMonoBehaviour
    {
		public bool HasFinishedRegenerating { get; private set; } = true;
		public bool IsRegenerating { get; private set; } = false;

		[SerializeField] private Health health = default;
		[Space]

        public UnityEvent OnFinishRegeneration = default;
		[SerializeField] private FloatReference healthPerSecond = default;
        [SerializeField] private FloatReference regenerationTime = default;

        private float timer = 0f;

		private void OnValidate()
		{
			if (health != null) return;
			health = GetComponent<Health>();
		}

		private void Update()
        {
            RegenerationOverTime();
            HandleInvulnerableTimer();
        }

        private void HandleInvulnerableTimer()
        {
            if (timer <= 0f)
            {
                if (HasFinishedRegenerating) return;

                if (OnFinishRegeneration != null)
                    OnFinishRegeneration.Invoke();
                return;
            }

            timer -= Time.deltaTime;
        }

        public void SetRegeneration()
        {
            if (regenerationTime <= 0f) return;

            timer = regenerationTime;
            IsRegenerating = true;
            HasFinishedRegenerating = false;
        }

        private void RegenerationOverTime()
        {
            float regenerate = System.Convert.ToSingle(IsRegenerating);
            float regeneration = regenerate * healthPerSecond * Time.deltaTime;
            Regenerate(regeneration);
        }

        public void Regenerate(float force)
        {
            health.Hurt(-force);
        }
    }
}