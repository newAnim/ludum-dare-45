using UnityEngine;
using UnityAtoms;
using TMPro;

namespace Game
{
    public class VariableTextMesh<T, T1> : MonoBehaviour where T : RefData, IValue<T1>
    {
        [SerializeField] private TextMeshProUGUI textMesh = default;
        [SerializeField] private T variable = default;

        ObserveUpdateChange<T1> observeUpdate = default;

        private void Awake()
        {
            observeUpdate.Assign(UpdateUI);
        }

        private void Update()
        {
            observeUpdate.Observe(variable.Value);
        }

        private void UpdateUI<U>(U value)
        {
            textMesh?.SetText(value.ToString());
        }
    }
}
