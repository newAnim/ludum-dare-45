using UltEvents;

namespace Game
{
    [System.Serializable]
    public class UltBoolEvent : UltEvent<bool> { }
}