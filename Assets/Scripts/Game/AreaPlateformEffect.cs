using UltEvents;
using UnityEngine;

namespace Game
{
    public class AreaPlateformEffect : MonoBehaviour
    {
        [SerializeField] LayerMask areaLayer = default;
        [SerializeField] bool keepPlateform = false;
        [SerializeField] bool destroyOnTrigger = true;
        [SerializeField] bool destroyPlateform = true;

        public UltEvent onDisable = default;

        private void OnTriggerEnter2D(Collider2D other)
        {
            if (other.gameObject == null) return;
            if (!areaLayer.Includes(other.gameObject.layer)) return;
            Plateform plateform = other.GetComponentInParent<Plateform>();
            if (keepPlateform)
                PlateformCreator.RemoveReference();

            if (destroyPlateform)
            {
                plateform?.Disable();
                PlateformCreator.RemoveReference();
            }

            if (destroyOnTrigger)
                onDisable?.Invoke();
        }
    }
}