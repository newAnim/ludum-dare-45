using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Game
{
    public class SceneChanger : MonoBehaviour
    {
        [SerializeField] Animator animator = default;
        [SerializeField] string paramaterTrigger = default;
        [SerializeField] Image progress = default;
        [SerializeField] float maxSpeed = 1f;
        [SerializeField] LoadSceneMode loadSceneMode = LoadSceneMode.Single;

        private int sceneIndex = 0;

        public void LoadScene(int value)
        {
            sceneIndex = value;
            animator?.SetTrigger(paramaterTrigger);
        }

        public void OnFadeComplete()
        {
            StartCoroutine(Loading(sceneIndex));
        }

        IEnumerator Loading(int value)
        {
            float fakeProgress = 0f;
            float velocity = 0f;
            float smoothTime = .3f;
            AsyncOperation async = SceneManager.LoadSceneAsync(value, loadSceneMode);
            async.allowSceneActivation = false;

            while (fakeProgress < .95f)
            {
                float targetProgress = async.progress;
                if (async.progress >= .9f)
                    targetProgress = 1f;
                fakeProgress = Mathf.SmoothDamp(fakeProgress, targetProgress, ref velocity, smoothTime, maxSpeed);
                if (progress != null && progress.type.Equals(Image.Type.Filled))
                    progress.fillAmount = fakeProgress;
                yield return null;
            }
            progress.fillAmount = 1f;
            async.allowSceneActivation = true;
            yield return null;
        }
    }
}