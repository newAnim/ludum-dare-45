using UltEvents;
using UnityEngine;

namespace Game
{
    public class Plateform : MonoBehaviour
    {
        public UltEvent onEnable = default;
        public UltEvent onDisable = default;

        private void OnEnable() => onEnable?.Invoke();

        public void Disable() => onDisable?.Invoke();
    }
}