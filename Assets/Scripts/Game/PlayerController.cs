using UltEvents;
using UnityEngine;
using static UnityEngine.InputSystem.InputAction;

namespace Game
{
    public class PlayerController : MonoBehaviour
    {
		[SerializeField] new Rigidbody2D rigidbody = default;
        [SerializeField] private int jump = 1;
		[Header("Speed")]
		[SerializeField] private float speed = 20f;
		[SerializeField] private float airSpeedMultiplier = .75f;
        [SerializeField] private float jumpForce = 40f;
        [Range(0, .3f)] [SerializeField] float movementSmoothing = default;
        [SerializeField] private float stopForceVelocity = .2f;
        [SerializeField] private float stopForceMultiplier = .5f;

        [Header("Wall")]
        [SerializeField] private float wallJumpForce = 25f;
		[SerializeField] private LayerMask wallLayer = default;
		[SerializeField] private Transform[] wallTransforms = default;
		[SerializeField] private float wallCheckRadius = .1f;

		[Header("Ground")]
		[SerializeField] private LayerMask groundLayer = default;
		[SerializeField] private Transform groundTransform = default;
		[SerializeField] private float groundCheckRadius = .1f;

        public UltEvent onJump = default;
        public UltEvent onWallJump = default;
        public UltBoolEvent onTouchGround = default;
        public UltBoolEvent onTouchWall = default;
        public UltBoolEvent onMoving = default;

        private Vector3 velocity = Vector3.zero;

        public int JumpCount { get; private set; } = 0;
        public float Move { get; private set; } = 0f;
        public bool Moving => Move != 0;
        public Vector2 Movement { get; private set; } = Vector2.zero;
        public bool Grounded { get; private set; } = false;
        public Collider2D Wall { get; private set; } = default;

        ObserveUpdateChange<bool> observeMovement = default;
        ObserveUpdateChange<bool> observeGrounded = default;
        ObserveUpdateChange<bool> observeWalled = default;

        private void Awake()
        {
            observeGrounded.Assign(OnGrounded);
            observeWalled.Assign(OnWalled);
            observeMovement.Assign(OnMoving);
        }

        private void Update()
        {
            observeMovement.Observe(Moving);
            observeGrounded.Observe(Grounded);
            observeWalled.Observe(Wall != null);
        }

        private void FixedUpdate()
        {
            HandleMovement();
            CheckGround();
			CheckWall();
		}

		private void OnDrawGizmos()
		{
			if (groundTransform == null) return;
			Gizmos.color = Color.cyan;
			Gizmos.DrawWireSphere(groundTransform.position, groundCheckRadius);
			for (int i = wallTransforms.Length - 1; i >= 0; i--)
			{
				Gizmos.DrawWireSphere(wallTransforms[i].position, wallCheckRadius);
			}
		}

        void OnMoving(bool value) => onMoving?.Invoke(value);

        void OnGrounded(bool value)
        {
            onTouchGround?.Invoke(value);

            if (value)
                ResetCountJump();
        }

        void OnWalled(bool value)
        {
            onTouchWall?.Invoke(value);

            if (value)
                ResetCountJump();
        }

        public void ResetCountJump()
        {
            JumpCount = jump;
        }

        public void SetMovement(CallbackContext context)
            => Move = context.ReadValue<float>();

        private void HandleMovement()
        {
            Movement = new Vector2(Move * speed, rigidbody.velocity.y);

            if (!Grounded)
                Movement = new Vector2(Movement.x * airSpeedMultiplier, Movement.y);

            if (Grounded && !Moving && Mathf.Abs(rigidbody.velocity.x) > stopForceVelocity)
                rigidbody.AddForce(-rigidbody.velocity * stopForceMultiplier);

            rigidbody.velocity = Vector3.SmoothDamp(rigidbody.velocity, Movement, ref velocity, movementSmoothing);
        }

		public void Jump(CallbackContext context)
		{
            if (context.started) return;
            if (JumpCount <= 0) return;
            JumpCount--;
            rigidbody.velocity = new Vector2(rigidbody.velocity.x, 0f);
            Vector2 jump = new Vector2(0, jumpForce);
			if (Wall != null)
			{
				var rawDir = transform.position - Wall.transform.position;
                float dir = rawDir.x > 0f ? 1f : -1f;
                jump.x = dir * wallJumpForce;
                rigidbody?.AddForce(jump, ForceMode2D.Impulse);
                onWallJump?.Invoke();
                return;
            }

            onJump?.Invoke();
			rigidbody?.AddForce(jump, ForceMode2D.Impulse);
		}

        private void CheckGround()
        {
            Grounded = Physics2D.OverlapCircle(groundTransform.position, groundCheckRadius, groundLayer);
        }

        private void CheckWall()
		{
			Wall = OverlapAll();
		}

		Collider2D OverlapAll()
		{
			for (int i = wallTransforms.Length - 1; i >= 0; i--)
			{
				Collider2D collider2D = Physics2D.OverlapCircle(wallTransforms[i].position, wallCheckRadius, wallLayer);
				if (collider2D)
					return collider2D;
			}
			return null;
		}
	}
}