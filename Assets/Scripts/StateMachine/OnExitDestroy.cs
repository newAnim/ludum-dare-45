using UnityEngine;

namespace StateMachine
{
    public class OnExitDestroy : StateMachineBehaviour
    {
        [SerializeField] float seconds = 0f;

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            if(seconds <= 0)
            {
                Destroy(animator.gameObject);
                return;
            }

            Destroy(animator.gameObject, seconds);
        }
    }
}