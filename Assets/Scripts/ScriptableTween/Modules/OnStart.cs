﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityAtoms;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;

	[CreateAssetMenu(fileName = nameof(OnStart), menuName = Modules + nameof(OnStart), order = Order)]
	public class OnStart : TweenModule
	{
		[SerializeField] VoidEvent onStart = default;
		[SerializeField] VoidAction[] onStartActions = default;

		protected override void Set(Tweener tweener)
		{
			base.Set(tweener);
			tweener.OnStart(Trigger);
		}

		void Trigger()
		{
			onStart?.Raise();
			int length = onStartActions.Length;
			for (int i = 0; i < length; i++)
			{
				onStartActions[i]?.Do();
			}
		}
	}
}