﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(TimeScale), menuName = Modules + nameof(TimeScale), order = Order)]
	public class TimeScale : TweenModule
	{
		[SerializeField] float tweenTimeScale = 1;

		protected override void Set(Tweener tweener)
		{
			base.Set(tweener);
			if (tweener != null)
				tweener.timeScale = tweenTimeScale;
		}
	}
}