﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityEngine;

namespace ScriptableTween
{
	public abstract class TweenModule : ScriptableObject
	{
		[Multiline]
		public string developerDescription = "";

		public ScriptableObjectTween tween = default;

		private void OnValidate()
		{
			AddToTween();
		}

		protected void OnEnable() => Subscribe();

		protected void OnDisable() => Unsubscribe();

		public void Subscribe()
		{
			if (tween == null) return;
			tween.onDo += Set;
		}

		public void Unsubscribe()
		{
			if (tween == null) return;
			tween.onDo -= Set;
		}

		public void AddToTween()
		{
			if (tween?.tweenModules.Contains(this) ?? true) return;
			tween.tweenModules.Add(this);
		}

		protected virtual void Set(Tweener tweener) { }
	}
}