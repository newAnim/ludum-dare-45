﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(Loops), menuName = Modules + nameof(Loops), order = Order)]
	public class Loops : TweenModule
	{
		[SerializeField] int loops = 1;
		[SerializeField] LoopType loopType = LoopType.Restart;

		protected override void Set(Tweener tweener)
		{
			base.Set(tweener);
			tweener.SetLoops(loops, loopType);
		}
	}
}