﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityAtoms;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(DOBlendableMove), menuName = Main + nameof(DOBlendableMove), order = Order)]
	public class DOBlendableMove : ScriptableObjectTween
	{
		[SerializeField] Vector3Reference byValue = default;
		[SerializeField] FloatReference duration = default;
		[SerializeField] BoolReference snapping = default;

		public override void Do(Transform transform) => Do(transform?.DOBlendableMoveBy(byValue, duration, snapping));
	}
}