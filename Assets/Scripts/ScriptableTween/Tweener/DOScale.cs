﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityAtoms;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(DOScale), menuName = Main + nameof(DOScale), order = Order)]
	public class DOScale : ScriptableObjectTween
	{
		[SerializeField] Vector3Reference endValue = default;
		[SerializeField] FloatReference duration = default;

		public override void Do(Transform transform) => Do(transform?.DOScale(endValue, duration));
	}
}