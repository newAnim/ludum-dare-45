﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityAtoms;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(DOBlendableScale), menuName = Main + nameof(DOBlendableScale), order = Order)]
	public class DOBlendableScale : ScriptableObjectTween
	{
		[SerializeField] Vector3Reference byValue = default;
		[SerializeField] FloatReference duration = default;

		public override void Do(Transform transform) => Do(transform?.DOBlendableScaleBy(byValue, duration));
	}
}