﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using System;
using System.Collections.Generic;
using UnityAtoms;
using UnityEngine;

namespace ScriptableTween
{
	public abstract class ScriptableObjectTween : ScriptableObject, ISerializationCallbackReceiver
	{
		[Multiline]
		public string developerDescription = "";

		public Tweener tweener = default;
		public Action<Tweener> onDo = default;
		[HideInInspector] public List<TweenModule> tweenModules = new List<TweenModule>();

		private void OnValidate() => RemoveUnlinkedTweenModules();

		public void OnBeforeSerialize() => RemoveUnlinkedTweenModules();

		public void OnAfterDeserialize() { }

		protected virtual void RemoveUnlinkedTweenModules()
		{
			int length = tweenModules.Count - 1;
			for (int i = length; i >= 0; i--)
			{
				if (!tweenModules[i].tween?.Equals(this) ?? true)
					tweenModules.RemoveAt(i);
			}
		}

		public virtual void Do(Tweener tweener)
		{
			this.tweener = tweener;
			onDo?.Invoke(tweener);
		}

		public virtual void Do(Transform transform) { }
	}
}